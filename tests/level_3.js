/* eslint no-underscore-dangle: "off" */
/* eslint no-param-reassign: "off" */

mocha.setup('bdd');
const assert = chai.assert;

/*
* Tests
*/

describe('Создание смарт-объекта и определение вычисляемых свойств', () => {
  describe('1. Создание смарт-объекта - глубокое копирование свойств', () => {
    let nestedObj = {};
    let smartObj = {};
    let newObjProp = {};

    beforeEach(() => {
      nestedObj = {
        main: {
          changeable: {
            get a() {
              return this._a;
            },
            set a(value) {
              this._a = value;
            },
            b: 2,
            c: 3,
            _a: undefined,
          },
          ab: 22,
          ac: 23,
        },
        someNum1: 222,
        someNum2: 333,
      };

      Object.defineProperty(nestedObj, 'non_writable', {
        value: 'nw',
        writable: false,
      });

      Object.defineProperty(nestedObj, 'enumerable', {
        value: 'e',
        enumerable: true,
      });

      Object.defineProperty(nestedObj.main, 'configurable', {
        value: 'c',
        configurable: true,
      });

      Object.defineProperty(nestedObj.main, 'non_writable', {
        value: 'nwn',
        writable: false,
      });

      Object.defineProperty(nestedObj.main, 'enumerable', {
        value: 'e',
        enumerable: true,
      });

      Object.defineProperty(nestedObj, 'configurable', {
        value: 'c',
        configurable: true,
      });

      nestedObj.main.changeable.a = 1;
      smartObj = createSmartObject(nestedObj);
    });

    it('1.1 Глубокое копирование структуры объекта и значений свойств при создании смарт-объекта', () => {
      assert.isTrue(
        smartObj.main.changeable.a === 1 &&
          smartObj.main.changeable.b === 2 &&
          smartObj.main.changeable.c === 3 &&
          smartObj.main.changeable._a === 1 &&
          smartObj.main.ab === 22 &&
          smartObj.main.ac === 23 &&
          smartObj.someNum1 === 222 &&
          smartObj.someNum2 === 333,
      );
    });

    it('1.2 Смарт-объект, полученный копированием массива, имеет прототип "массив"', () => {
      const obj = [1, 2, 3];
      const smartObj = createSmartObject(obj);

      assert.isTrue(Array.isArray(Object.getPrototypeOf(smartObj)));
    });

    it('1.2 Смарт-объект, полученный копированием массива, сохраняет метод map', () => {
      const obj = [1, 2, 3];
      const smartObj = createSmartObject(obj);
      const newArray = smartObj.map(item => item * 2);

      assert.isTrue(
        newArray[0] === 2 && newArray[1] === 4 && newArray[2] === 6,
      );
    });

    it('1.3 Смарт-объект, полученный копированием объекта с заданным прототипом, сохраняет унаследованные свойства', () => {
      const protoObj = { inhdProp: 'proto' };
      const newObj = { ownProp: 'own' };
      Object.setPrototypeOf(newObj, protoObj);

      const smartObj = createSmartObject(newObj);

      assert.strictEqual(smartObj.inhdProp, 'proto');
    });

    it('1.4 Смарт-объект содержит унаследованные методы', () => {
      const obj = {
        a: 1,
        inhdMethod() {
          return this.a * 2;
        },
      };

      const smartObj = createSmartObject(obj);
      smartObj.a = 2;
      assert.strictEqual(smartObj.inhdMethod(), 4);
    });

    it('1.5.1 Скопированное свойство, имевшее геттер и сеттер, - присваивание значения-примитива', () => {
      smartObj.main.changeable.a = 10;
      assert.strictEqual(smartObj.main.changeable.a, 10);
    });
    it('1.5.2 Скопированное свойство, имевшее геттер и сеттер, - присваивание null', () => {
      smartObj.main.changeable.a = null;
      assert.strictEqual(smartObj.main.changeable.a, null);
    });
    it('1.5.3 Скопированное свойство, имевшее геттер и сеттер, - присваивание NaN', () => {
      smartObj.main.changeable.a = NaN;
      assert.isTrue(Number.isNaN(smartObj.main.changeable.a));
    });

    it('1.5.4 Скопированное свойство, имевшее геттер и сеттер, - присваивание массива - содержит свойство length', () => {
      smartObj.main.changeable.a = [1, 2, 3];
      assert.isTrue(smartObj.main.changeable.a.length === 3);
    });

    it('1.5.4 Скопированное свойство, имевшее геттер и сеттер, - присваивание массива - содержит метод reduce', () => {
      smartObj.main.changeable.a = [1, 2, 3];
      assert.strictEqual(
        smartObj.main.changeable.a.reduce((acc, x) => acc + x),
        6,
      );
    });

    it('1.5.5 Скопированное свойство, имевшее геттер и сеттер, - присваивание массива - содержит метод map', () => {
      smartObj.main.changeable.a = [1, 2, 3];
      const newArray = smartObj.main.changeable.a.map(x => 2 * x);
      assert.strictEqual(
        newArray[0] === 2 && newArray[1] === 4 && newArray[2],
        6,
      );
    });

    it('1.6.1.1 Клонирование атрибутов свойств - без вложенности - non-writable - значение скопировано в смарт-объект', () => {
      assert.strictEqual(smartObj.non_writable, 'nw');
    });
    it('1.6.1.1 Клонирование атрибутов свойств - без вложенности - non-writable - задание нового значения невозможно', () => {
      try {
        smartObj.non_writable = 'w';
      } catch (e) {}
      assert.strictEqual(smartObj.non_writable, 'nw');
    });
    it('1.6.1.2 Клонирование атрибутов свойств - без вложенности - enumerable: true', () => {
      let d = Reflect.getOwnPropertyDescriptor(smartObj, 'enumerable');
      assert.isTrue(d.enumerable);
    });
    it('1.6.1.3 Клонирование атрибутов свойств - без вложенности - configurable: true', () => {
      let d = Reflect.getOwnPropertyDescriptor(smartObj, 'configurable');
      assert.isTrue(d.configurable);
    });

    it('1.6.2.1 Клонирование атрибутов свойств - вложенное свойство - non-writable - значение скопировано в смарт-объект', () => {
      assert.strictEqual(smartObj.main.non_writable, 'nwn');
    });
    it('1.6.2.1 Клонирование атрибутов свойств - вложенное свойство - non-writable - задание нового значения невозможно', () => {
      try {
        smartObj.main.non_writable = 'w';
      } catch (e) {}
      assert.strictEqual(smartObj.main.non_writable, 'nwn');
    });
    it('1.6.2.2 Клонирование атрибутов свойств - вложенное свойство - enumerable: true', () => {
      let d = Reflect.getOwnPropertyDescriptor(smartObj.main, 'enumerable');
      assert.isTrue(d.enumerable);
    });
    it('1.6.2.3 Клонирование атрибутов свойств - вложенное свойство - configurable: true', () => {
      let d = Reflect.getOwnPropertyDescriptor(smartObj.main, 'configurable');
      assert.isTrue(d.configurable);
    });
  });

  describe('2. Определение вычисляемых полей', () => {
    let nestedObj = {};
    let smartObj = {};
    let newObjProp = {};
    const testLog = [];

    beforeEach(() => {
      nestedObj = {
        main: {
          changeable: {
            get a() {
              return this._a;
            },
            set a(value) {
              this._a = value;
              this._aa = value + 1;
            },
            b: 2,
            c: 3,
          },
          ab: 22,
          ac: 23,
        },
        someNum1: 222,
        someNum2: 333,
        ifComp: 0,
      };

      nestedObj.main.changeable.a = 1;
      smartObj = createSmartObject(nestedObj);

      testLog.length = 0;

      defineComputedField(smartObj, 'comp1', data => {
        testLog.push(1);
        if (data.ifComp === 0) return data.main.changeable.a;
        return data.main.ab;
      });

      defineComputedField(smartObj, 'comp2', data => {
        testLog.push(2);
        if (data.ifComp === 0) return data.main.changeable.b;
        return data.main.ac;
      });
    });

    it('2.a Вычисляемое поле 1 рассчитано правильно', () => {
      assert.strictEqual(smartObj.comp1, 1);
    });

    it('2.a Вычисляемое поле 2 рассчитано правильно', () => {
      assert.strictEqual(smartObj.comp2, 2);
    });

    it('2.b Рассчитывается только одно вычисляемое поле', () => {
      testLog.length = 0;
      smartObj.main.changeable.a = 5;
      assert.isTrue(
        smartObj.comp1 === 5 && testLog.length === 1 && testLog[0] === 1,
      );
    });

    it('2.b Рассчитываются оба вычисляемых поля', () => {
      testLog.length = 0;
      smartObj.ifComp = 1;
      assert.isTrue(
        smartObj.comp1 === 22 &&
          smartObj.comp2 === 23 &&
          testLog.length === 2 &&
          testLog.indexOf(1) >= 0 &&
          testLog.indexOf(2) >= 0,
      );
    });

    it('2.b Ни одно из вычисляемых полей не рассчитывается', () => {
      testLog.length = 0;
      smartObj.someNum1 = 1;
      assert.isTrue(
        smartObj.comp1 === 1 && smartObj.comp2 === 2 && testLog.length === 0,
      );
    });

    it('2.c Сеттер свойства изменяет значения двух "внутренних" свойств, при этом обновляются зависящие от них вычисляемые поля', () => {
      defineComputedField(smartObj, 'comp_a', data => {
        testLog.push(11);
        return data.main.changeable._a;
      });

      defineComputedField(smartObj, 'comp_aa', data => {
        testLog.push(12);
        return data.main.changeable._aa;
      });

      testLog.length = 0;
      smartObj.main.changeable.a = 2;
      assert.isTrue(
        testLog.indexOf(11) >= 0 &&
          testLog.indexOf(12) >= 0 &&
          smartObj.comp_a === 2 &&
          smartObj.comp_aa === 3,
      );
    });
  });

  describe('3. Присвоение свойству объекта значения-массива (тесты из п. 1 для созданной ветви)', () => {
    let nestedObj = {};
    let smartObj = {};
    let newObjProp = {};
    const testLog = [];

    beforeEach(() => {
      nestedObj = {
        main: {
          changeable: {
            get a() {
              return this._a;
            },
            set a(value) {
              this._a = value;
              this._aa = value + 1;
            },
            non_writable: 'nw',
            enumerable: 'e',
            configurable: 'c',
            b: 2,
            c: 3,
            _a: undefined,
            _aa: undefined,
          },
          ab: 22,
          ac: 23,
        },
        someNum1: 222,
        someNum2: 333,
        ifComp: 0,
      };

      newObjProp = {
        get a() {
          return this._a;
        },
        set a(value) {
          this._a = value;
          this._aa = value + 1;
        },
        b: 2,
        c: 3,
        _a: undefined,
        _aa: undefined,
      };

      Object.defineProperty(newObjProp, 'non_writable', {
        value: 'nw',
        writable: false,
      });

      Object.defineProperty(newObjProp, 'enumerable', {
        value: 'e',
        enumerable: true,
      });

      Object.defineProperty(newObjProp, 'configurable', {
        value: 'c',
        configurable: true,
      });

      nestedObj.main.changeable.a = 1;
      newObjProp.a = 1;
      smartObj = createSmartObject(nestedObj);

      defineComputedField(smartObj, 'comp1', data => {
        testLog.push(1);
        if (data.ifComp === 0) return data.main.changeable.a;
        return data.main.ab;
      });

      defineComputedField(smartObj, 'comp2', data => {
        testLog.push(2);
        if (data.ifComp === 0) return data.main.changeable.b;
        return data.main.ac;
      });

      smartObj.main.changeable = 1;
      smartObj.main.changeable = newObjProp;
    });

    it('3.2.1 Скопированное свойство, имевшее геттер и сеттер, - присваивание значения-примитива', () => {
      smartObj.main.changeable.a = 10;
      assert.strictEqual(smartObj.main.changeable.a, 10);
    });
    it('3.2.2 Скопированное свойство, имевшее геттер и сеттер, - присваивание null', () => {
      smartObj.main.changeable.a = null;
      assert.strictEqual(smartObj.main.changeable.a, null);
    });
    it('3.2.3 Скопированное свойство, имевшее геттер и сеттер, - присваивание NaN', () => {
      smartObj.main.changeable.a = NaN;
      assert.isTrue(Number.isNaN(smartObj.main.changeable.a));
    });

    it('3.2.4 Скопированное свойство, имевшее геттер и сеттер, - присваивание массива - содержит свойство length', () => {
      smartObj.main.changeable.a = [1, 2, 3];
      assert.strictEqual(smartObj.main.changeable.a.length, 3);
    });

    it('3.2.5 Скопированное свойство, имевшее геттер и сеттер, - присваивание массива - содержит метод reduce', () => {
      smartObj.main.changeable.a = [1, 2, 3];
      assert.strictEqual(
        smartObj.main.changeable.a.reduce((acc, x) => acc + x),
        6,
      );
    });

    it('3.2.6 Скопированное свойство, имевшее геттер и сеттер, - присваивание массива - содержит метод map', () => {
      smartObj.main.changeable.a = [1, 2, 3];
      const newArray = smartObj.main.changeable.a.map(x => 2 * x);
      assert.strictEqual(
        newArray[0] === 2 && newArray[1] === 4 && newArray[2],
        6,
      );
    });

    it('3.3.1 Клонирование атрибутов свойств - non-writable - значение скопировано в смарт-объект', () => {
      assert.strictEqual(smartObj.main.changeable.non_writable, 'nw');
    });
    it('3.3.1 Клонирование атрибутов свойств - non-writable - задание нового значения невозможно', () => {
      try {
        smartObj.main.changeable.non_writable = 'w';
      } catch (e) {}
      assert.strictEqual(smartObj.main.changeable.non_writable, 'nw');
    });
    it('3.3.2 Клонирование атрибутов свойств - enumerable: true', () => {
      let d = Reflect.getOwnPropertyDescriptor(
        smartObj.main.changeable,
        'enumerable',
      );
      assert.isTrue(d.enumerable);
    });
    it('3.3.3 Клонирование атрибутов свойств - configurable: true', () => {
      let d = Reflect.getOwnPropertyDescriptor(
        smartObj.main.changeable,
        'configurable',
      );
      assert.isTrue(d.configurable);
    });

    it('3.4.1 Вычисляемое поле 1 рассчитано правильно', () => {
      assert.strictEqual(smartObj.comp1, 1);
    });

    it('3.4.1 Вычисляемое поле 2 рассчитано правильно', () => {
      assert.strictEqual(smartObj.comp2, 2);
    });

    it('3.4.2 Рассчитывается только одно вычисляемое поле', () => {
      testLog.length = 0;
      smartObj.main.changeable.a = 5;
      assert.isTrue(
        smartObj.comp1 === 5 && testLog.length === 1 && testLog[0] === 1,
      );
    });

    it('3.4.2 Рассчитываются оба вычисляемых поля', () => {
      testLog.length = 0;
      smartObj.ifComp = 1;
      assert.isTrue(
        smartObj.comp1 === 22 &&
          smartObj.comp2 === 23 &&
          testLog.length === 2 &&
          testLog.indexOf(1) >= 0 &&
          testLog.indexOf(2) >= 0,
      );
    });

    it('3.4.2 Ни одно из вычисляемых полей не рассчитывается', () => {
      testLog.length = 0;
      smartObj.someNum1 = 1;
      assert.isTrue(
        smartObj.comp1 === 1 && smartObj.comp2 === 2 && testLog.length === 0,
      );
    });

    it('3.4.3 Сеттер свойства изменяет значения двух "внутренних" свойств, при этом обновляются зависящие от них вычисляемые поля', () => {
      defineComputedField(smartObj, 'comp_a', data => {
        testLog.push(11);
        return data.main.changeable._a;
      });

      defineComputedField(smartObj, 'comp_aa', data => {
        testLog.push(12);
        return data.main.changeable._aa;
      });

      testLog.length = 0;
      smartObj.main.changeable.a = 2;
      assert.isTrue(
        testLog.indexOf(11) >= 0 &&
          testLog.indexOf(12) >= 0 &&
          smartObj.comp_a === 2 &&
          smartObj.comp_aa === 3,
      );
    });
  });

  describe('4. Присваивание значения-примитива свойству, имевшему тип "объект"', () => {
    let nestedObj = {};
    let smartObj = {};
    let newObjProp = {};
    const testLog = [];

    beforeEach(() => {
      nestedObj = {
        main: {
          changeable: {
            get a() {
              return this._a;
            },
            set a(value) {
              this._a = value;
              this._aa = value + 1;
            },
            non_writable: 'nw',
            enumerable: 'e',
            configurable: 'c',
            b: 2,
            c: 3,
            _a: undefined,
            _aa: undefined,
          },
          ab: 22,
          ac: 23,
        },
        someNum1: 222,
        someNum2: 333,
        ifComp: 0,
      };

      smartObj = createSmartObject(nestedObj);

      defineComputedField(smartObj, 'comp1', data => {
        testLog.push('1');
        return data.main.changeable;
      });

      smartObj.main.changeable = 1;
    });

    it('4.1 После присваивания значения-примитива - может быть присвоено и получено новое значение', () => {
      smartObj.main.changeable = 10;
      assert.strictEqual(smartObj.main.changeable, 10);
    });

    it('4.2 Зависящее от этого свойства вычисляемое поле рассчитывается правильно', () => {
      assert.strictEqual(smartObj.comp1, 1);
    });

    it('4.2 Вычисляемое поле обновляется при измении значения поля, которому было присвоено значение-примитив', () => {
      testLog.length = 0;
      smartObj.main.changeable = 10;
      assert.isTrue(smartObj.comp1 === 10 && testLog.length == 1);
    });
  });

  describe('5. Вычисляемые поля, зависящие от обычных и вычисляемых полей', () => {
    let oldObj = {};
    let smartObj = {};
    let newObjProp = {};
    const testLog = [];

    beforeEach(() => {
      oldObj = { a: '1', b: '2', c: '3', d: '4', e: '5', f: '6', ifComp: 0 };
      smartObj = createSmartObject(oldObj);

      defineComputedField(smartObj, 'comp1', data => {
        testLog.push('1');
        if (data.ifComp === 1) return data.comp5 + data.a;
        return data.a + data.b;
      });

      defineComputedField(smartObj, 'comp2', data => {
        testLog.push('2');
        if (data.ifComp === 1) return data.comp4 + data.c;
        return data.comp1 + data.c;
      });

      defineComputedField(smartObj, 'comp3', data => {
        testLog.push('3');
        if (data.ifComp === 1) return data.f + data.e;
        return data.comp2 + data.d;
      });

      defineComputedField(smartObj, 'comp4', data => {
        testLog.push('4');
        if (data.ifComp === 1) return data.comp3 + data.d;
        return data.comp3 + data.e;
      });

      defineComputedField(smartObj, 'comp5', data => {
        testLog.push('5');
        if (data.ifComp === 1) return data.comp2 + data.b;
        return data.comp4 + data.f;
      });
    });

    it('5.1 Вычисляемые свойства правильно рассчитаны после определения (5 вычисляемых свойств, требуемый порядок 1-2-3-4-5)', () => {
      assert.isTrue(
        smartObj.comp1 === '12' &&
          smartObj.comp2 === '123' &&
          smartObj.comp3 === '1234' &&
          smartObj.comp4 === '12345' &&
          smartObj.comp5 === '123456',
      );
    });

    it('5.2 Вычисляемые поля получают правильные значения после полного изменения порядка расчета, без лишних вызовов callback-функций (3-4-2-5-1)', () => {
      testLog.length = 0;
      smartObj.ifComp = 1;
      assert.isTrue(
        testLog.length <= 9 &&
          smartObj.comp1 === '654321' &&
          smartObj.comp2 === '6543' &&
          smartObj.comp3 === '65' &&
          smartObj.comp4 === '654' &&
          smartObj.comp5 === '65432',
      );
    });

    it('5.2 Вычисляемые поля получают правильные значения при частичном пересчете, без лишних вызовов callback-функций (2-5-1)', () => {
      smartObj.ifComp = 1;
      testLog.length = 0;
      smartObj.c = '0';
      assert.isTrue(
        testLog.length === 3 &&
          smartObj.comp1 === '654021' &&
          smartObj.comp2 === '6540' &&
          smartObj.comp3 === '65' &&
          smartObj.comp4 === '654' &&
          smartObj.comp5 === '65402',
      );
    });
  });
});

/*
* Run tests
*/

mocha.run();
