/* eslint no-underscore-dangle: "off" */
/* eslint no-param-reassign: "off" */

mocha.setup('bdd');
const assert = chai.assert;

/*
* Tests
*/

describe('createSmartObject', () => {
  it('создание объекта с "умной" функциональностью" с копированием исходных свойств', () => {
    const obj = createSmartObject({
      name: 'Vasya',
      surname: 'Ivanov',
      patronymic: 'Olegovich',
    });
    assert.equal(obj.name, 'Vasya');
    assert.equal(obj.surname, 'Ivanov');
    assert.equal(obj.patronymic, 'Olegovich');
  });

  it('созданный объект с "умной" функциональностью", не зависит от исходного объекта', () => {
    const obj1 = {
      name: 'Vasya',
      surname: 'Ivanov',
      patronymic: 'Olegovich',
    };
    const obj = createSmartObject(obj1);
    obj.name = 'Ivan';
    obj1.name = 'Sergey';
    assert.equal(obj.name, 'Ivan');
  });

  it('создание объекта с "умной" функциональностью" с копированием исходных свойств и добавлением нового свойства', () => {
    const obj = createSmartObject({
      name: 'Vasya',
      surname: 'Ivanov',
      patronymic: 'Olegovich',
    });
    obj.age = 25;
    assert.equal(obj.age, 25);
  });

  it('создание объекта с "умной" функциональностью", где в исходном объекте есть геттер', () => {
    const obj = createSmartObject({
      name: 'Vasya',
      surname: 'Ivanov',
      patronymic: 'Olegovich',
      get fio() {
        return `${this.surname} ${this.name[0]}. ${this.patronymic[0]}.`;
      },
    });
    assert.equal(obj.fio, 'Ivanov V. O.');
  });

  it('создание объекта с "умной" функциональностью", где в исходном объекте есть геттер, и изменение этого свойства', () => {
    const obj = createSmartObject({
      name: 'Vasya',
      surname: 'Ivanov',
      patronymic: 'Olegovich',
      get fio() {
        return `${this.surname} ${this.name[0]}. ${this.patronymic[0]}.`;
      },
    });
    assert.equal(obj.fio, 'Ivanov V. O.');
    obj.surname = 'Popov';
    assert.equal(obj.fio, 'Popov V. O.');
    obj.fio = 'Novikov S. S.';
    assert.equal(obj.fio, 'Popov V. O.');
  });
});

describe('defineComputedField', () => {
  it('добавление в объект "умного" поля', () => {
    const obj = createSmartObject({
      name: 'Vasya',
      surname: 'Ivanov',
      patronymic: 'Olegovich',
    });
    defineComputedField(
      obj,
      'fullName',
      data => `${data.name} ${data.surname} ${data.patronymic}`,
    );
    assert.equal(obj.fullName, 'Vasya Ivanov Olegovich');
  });

  it('добавление в объект "умного" поля и изменение "умного" поля после изменения свойства объекта', () => {
    const obj = createSmartObject({
      name: 'Vasya',
      surname: 'Ivanov',
      patronymic: 'Olegovich',
    });
    defineComputedField(
      obj,
      'fullName',
      data => `${data.name} ${data.patronymic}`,
    );
    obj.name = 'Sergey';
    assert.equal(obj.fullName, 'Sergey Olegovich');
  });

  it('вывод ошибки при попытке прямого изменения "умного" поля объекта', () => {
    const obj = createSmartObject({
      name: 'Vasya',
      surname: 'Ivanov',
      patronymic: 'Olegovich',
    });
    defineComputedField(
      obj,
      'fullName',
      data => `${data.name} ${data.surname} ${data.patronymic}`,
    );
    function changeSmartField() {
      obj.fullName = 'Sergey Ivanov Olegovich';
    }
    assert.throws(changeSmartField, 'Assignment to computed property');
  });

  it('"умное" поле не обновляет своё значение при изменении свойства, которое не относится к "умной" функции', () => {
    let changes = 0;
    const obj = createSmartObject({
      name: 'Vasya',
      surname: 'Ivanov',
      patronymic: 'Olegovich',
    });
    obj.other = 'field'; // changes = 0
    assert.equal(changes, 0);
    obj.other = 'other field'; // changes = 0
    defineComputedField(obj, 'fullName', data => {
      changes += 1;
      return `${data.name} ${data.surname} ${data.patronymic}`;
    });
    assert.equal(changes, 1);
    obj.name = 'Andrey'; // changes = 2
    assert.equal(changes, 2);
    obj.other = 'other field changed'; // changes = 2
    assert.equal(changes, 2);
    obj.name = 'Sergey'; // changes = 3
    assert.equal(changes, 3);
  });

  it('добавление в объект ДВА "умных" независимых поля с использованием смежных свойств', () => {
    const changes = {
      fn1: 0,
      fn2: 0,
    };
    const obj = createSmartObject({
      name: 'Vasya',
      surname: 'Ivanov',
      patronymic: 'Olegovich',
      birthday: '09.09.1999',
    });
    assert.equal(changes.fn1, 0);
    assert.equal(changes.fn2, 0);
    defineComputedField(obj, 'fullName', data => {
      changes.fn1 += 1;
      return `${data.name} ${data.surname} ${data.patronymic}`;
    });
    assert.equal(changes.fn1, 1);
    assert.equal(changes.fn2, 0);
    defineComputedField(obj, 'age', data => {
      changes.fn2 += 1;
      // const age = Date() - new Date(data.birthday);
      return `${data.name} ${data.surname}, ${data.birthday} years old.`;
    });
    assert.equal(changes.fn1, 1);
    assert.equal(changes.fn2, 1);
    obj.name = 'Potap'; // call function 1 and 2
    assert.equal(changes.fn1, 2);
    assert.equal(changes.fn2, 2);
    obj.patronymic = 'Nikolaevich'; // call function 1
    assert.equal(changes.fn1, 3);
    assert.equal(changes.fn2, 2);
    obj.birthday = '10.10.2000'; // call function 2
    assert.equal(changes.fn1, 3);
    assert.equal(changes.fn2, 3);
  });

  it('проверка работы функции, которая использует разные свойства объекта в зависимости от их значений', () => {
    let changes = 0;
    const man = createSmartObject({
      name: 'Potap',
      surname: 'Ivanov',
      patronymic: 'Olegovich',
      age: 23,
    });
    assert.equal(changes, 0);

    defineComputedField(man, 'greeting', data => {
      changes += 1;
      if (data.age > 30) return `Hello ${data.name} ${data.patronymic}!`;
      return `Hello ${data.name}!`;
    });
    assert.equal(changes, 1);

    man.name = 'Andrey';
    assert.equal(changes, 2);
    assert.equal(man.greeting, 'Hello Andrey!');

    man.patronymic = 'Maksimovich';
    assert.equal(changes, 2);
    assert.equal(man.greeting, 'Hello Andrey!');

    man.age = 35;
    assert.equal(changes, 3);
    assert.equal(man.greeting, 'Hello Andrey Maksimovich!');

    man.patronymic = 'Olegovich';
    assert.equal(changes, 4);
    assert.equal(man.greeting, 'Hello Andrey Olegovich!');
  });

  it('Копирование полей с getter/setter. Параметры полей.', () => {
    let callCount = 0;
    const oldObj = {
      a: 'a, ',
      b: 'b, ',
      c: 'c, ',
      get g() {
        return this._g + this.c;
      },
      set g(val) {
        this._g = val;
      },
    };

    oldObj.g = ' bad_test, ';

    const obj = createSmartObject(oldObj);

    defineComputedField(obj, 'abg', data => {
      callCount += 1;
      return data.a + data.b + data.g;
    });
    assert.equal(callCount, 1);
    assert.equal(obj.abg, 'a, b,  bad_test, c, ');

    obj.g = 'test getter, ';
    assert.equal(callCount, 2);
    assert.equal(obj.abg, 'a, b, test getter, c, ');

    obj.a = 'test a, ';
    assert.equal(callCount, 3);
    assert.equal(obj.abg, 'test a, b, test getter, c, ');

    obj.c = 'good job; ';
    assert.equal(callCount, 4);
    assert.equal(obj.abg, 'test a, b, test getter, good job; ');

    assert.equal(callCount, 4);
    assert.equal(obj.abg, 'test a, b, test getter, good job; ');
  });

  it('Разрешение зависимостей вычисляемых свойств друг от друга', () => {
    const oldObj = {
      a: 'a ',
      b: 'b ',
      c: 'c ',
      ifComp: false,
    };

    const obj = createSmartObject(oldObj);

    defineComputedField(obj, 'comp1', data => {
      if (data.ifComp) return data.comp3;
      return data.a + data.b;
    });

    defineComputedField(obj, 'comp2', data => data.comp1 + data.c);

    defineComputedField(obj, 'comp3', data => {
      if (data.ifComp) return 'comp3 ';
      return data.comp2;
    });

    assert.equal(obj.comp1, 'a b ');
    assert.equal(obj.comp2, 'a b c ');
    assert.equal(obj.comp3, 'a b c ');

    obj.ifComp = 1;

    assert.equal(obj.comp1, 'comp3 ');
    assert.equal(obj.comp2, 'comp3 c ');
    assert.equal(obj.comp3, 'comp3 ');
  });

  it('Проверка количества вызовов функций при разрешении зависимостей вычисляемых свойств друг от друга', () => {
    const callCount = {
      call_1: 0,
      call_2: 0,
      call_3: 0,
    };

    const oldObj = {
      a: 'a ',
      b: 'b ',
      c: 'c ',
      ifComp: false,
    };

    const obj = createSmartObject(oldObj);

    defineComputedField(obj, 'comp1', data => {
      callCount.call_1 += 1;
      if (data.ifComp) return data.comp3;
      return data.a + data.b;
    });
    assert.equal(callCount.call_1, 1);
    assert.equal(callCount.call_2, 0);
    assert.equal(callCount.call_3, 0);

    defineComputedField(obj, 'comp2', data => {
      callCount.call_2 += 1;
      return data.comp1 + data.c;
    });
    assert.equal(callCount.call_1, 1);
    assert.equal(callCount.call_2, 1);
    assert.equal(callCount.call_3, 0);

    defineComputedField(obj, 'comp3', data => {
      callCount.call_3 += 1;
      if (data.ifComp) return 'comp3 ';
      return data.comp2;
    });
    assert.equal(callCount.call_1, 1);
    assert.equal(callCount.call_2, 1);
    assert.equal(callCount.call_3, 1);

    obj.ifComp = 1;

    assert.equal(callCount.call_1, 3);
    assert.equal(callCount.call_2, 3);
    assert.equal(callCount.call_3, 2);
  });

  it('Должна возникать ошибка "read only" при попытке присваивания значения свойству с параметром writable = false', () => {
    const ob = {};
    Object.defineProperty(ob, 'pi', { value: 3.14159265 });
    const o = createSmartObject(ob);
    function changePropReadOnly() {
      o.pi = 1;
    }
    assert.throws(changePropReadOnly, 'read only');
  });

  it('Наследование прототипа', () => {
    const proto = { a: 5 };
    const oldObj = Object.create(proto);
    const o = createSmartObject(oldObj);

    assert.equal(oldObj.a, 5);
    assert.equal(o.a, 5);
  });

  it('Должны показываться только нужные поля', () => {
    const oldObj = { a: 1 };
    const o = createSmartObject(oldObj);

    assert.equal(Object.keys(oldObj).toString(), 'a');
    assert.equal(Object.keys(o).toString(), 'a');
  });

  it('Наследование осуществляется корректно даже для массива', () => {
    const oldObj = [1, 2, 3, 4, 5, 6];
    const obj = createSmartObject(oldObj);
    const oldObjResult = oldObj.reduce((a, b) => a + b);
    const objResult = obj.reduce((a, b) => a + b);

    assert.equal(oldObjResult, 21);
    assert.equal(objResult, 21);
  });
});

/*
* Run tests
*/

mocha.run();
