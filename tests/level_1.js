/* eslint no-underscore-dangle: "off" */
/* eslint no-param-reassign: "off" */

mocha.setup('bdd');
const assert = chai.assert;

/*
* Tests
*/

describe('createSmartObject', () => {
  it('создание объекта с помощью createSmartObject()', () => {
    const obj = createSmartObject({
      name: 'Vasya',
      surname: 'Ivanov',
      patronymic: 'Olegovich',
    });
    assert.equal(obj.name, 'Vasya');
    assert.equal(obj.surname, 'Ivanov');
    assert.equal(obj.patronymic, 'Olegovich');
  });

  it('создание объекта с помощью createSmartObject() и изменение его свойств', () => {
    const obj = createSmartObject({
      name: 'Vasya',
      surname: 'Ivanov',
      patronymic: 'Olegovich',
    });
    obj.name = 'Ivan';
    obj.surname = 'Nikolaevich';
    obj.patronymic = 'Petrov';
    obj.age = 25;
    assert.equal(obj.name, 'Ivan');
    assert.equal(obj.surname, 'Nikolaevich');
    assert.equal(obj.patronymic, 'Petrov');
    assert.equal(obj.age, 25);
  });

  it('создание объекта с помощью createSmartObject() и добавление свойств', () => {
    const obj = createSmartObject({
      name: 'Vasya',
      surname: 'Ivanov',
      patronymic: 'Olegovich',
    });
    obj.age = 25;
    assert.equal(obj.age, 25);
  });
});

describe('defineComputedField', () => {
  it('добавление в объект "умного" поля', () => {
    const obj = createSmartObject({
      name: 'Vasya',
      surname: 'Ivanov',
      patronymic: 'Olegovich',
    });
    defineComputedField(
      obj,
      'fullName',
      ['name', 'surname', 'patronymic'],
      (name, surname, patronymic) => `${name} ${surname} ${patronymic}`,
    );
    assert.equal(obj.fullName, 'Vasya Ivanov Olegovich');
  });

  it('добавление в объект "умного" поля и изменение "умного" поля после изменения свойства объекта', () => {
    const obj = createSmartObject({
      name: 'Vasya',
      surname: 'Ivanov',
      patronymic: 'Olegovich',
    });
    defineComputedField(
      obj,
      'fullName',
      ['name', 'surname', 'patronymic'],
      (name, surname, patronymic) => `${name} ${surname} ${patronymic}`,
    );
    obj.name = 'Sergey';
    assert.equal(obj.fullName, 'Sergey Ivanov Olegovich');
  });

  it('вывод ошибки при попытке прямого изменения "умного" поля объекта', () => {
    const obj = createSmartObject({
      name: 'Vasya',
      surname: 'Ivanov',
      patronymic: 'Olegovich',
    });
    defineComputedField(
      obj,
      'fullName',
      ['name', 'surname', 'patronymic'],
      (name, surname, patronymic) => `${name} ${surname} ${patronymic}`,
    );
    function changeSmartField() {
      obj.fullName = 'Sergey Ivanov Olegovich';
    }
    assert.throws(changeSmartField, 'error');
  });

  it('"умное" поле не обновляет своё значение при изменении свойства, которое не относится к "умной" функции', () => {
    let changes = 0;
    const obj = createSmartObject({
      name: 'Vasya',
      surname: 'Ivanov',
      patronymic: 'Olegovich',
    });
    obj.other = 'field'; // changes = 0
    assert.equal(changes, 0);
    obj.other = 'other field'; // changes = 0
    defineComputedField(
      obj,
      'fullName',
      ['name', 'surname', 'patronymic'],
      (name, surname, patronymic) => {
        changes += 1;
        return `${name} ${surname} ${patronymic}`;
      },
    );
    assert.equal(changes, 1);
    obj.name = 'Andrey'; // changes = 2
    assert.equal(changes, 2);
    obj.other = 'other field changed'; // changes = 2
    assert.equal(changes, 2);
    obj.name = 'Sergey'; // changes = 35
    assert.equal(changes, 3);
  });
});

/*
* Run tests
*/

mocha.run();
